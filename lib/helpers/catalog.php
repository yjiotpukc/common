<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 03.07.18
 * Time: 15:54
 */

namespace Oneway\Common\Helpers;


use Bitrix\Catalog\CatalogIblockTable;
use Bitrix\Catalog\ProductTable;
use Bitrix\Main\Loader;


final class Catalog
{
    /** @var array Локальный кэш каталогов */
    protected static $catalogs = [];

    /**
     * @var array [productId => type] Локальный кэш типов товаров
     * @see \Bitrix\Catalog\ProductTable спискок возможных типов товаров
     */
    protected static $productTypes = [];


    /**
     * Является ли инфоблок каталогом
     *
     * @param int $iblockId
     *
     * @return bool
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function isCatalog(int $iblockId): bool
    {
        self::loadCatalogs();

        return isset(static::$catalogs[$iblockId]);
    }


    /**
     * Является ли каталог каталогом продуктов
     *
     * @param int $iblockId
     *
     * @return bool
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function isProducts(int $iblockId): bool
    {
        self::loadCatalogs();

        return static::$catalogs[$iblockId]['IS_PRODUCTS'];
    }


    /**
     * Является ли каталог каталогом торговых предложений
     *
     * @param $iblockId
     *
     * @return bool
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function isOffers(int $iblockId): bool
    {
        self::loadCatalogs();

        return static::$catalogs[$iblockId]['IS_OFFERS'];
    }


    /**
     * Разделён ли каталог на товары и торговые предложения
     *
     * @param int $iblockId - можно передать ID ИБ ТП или товаров
     *
     * @return bool
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function isSeparated(int $iblockId): bool
    {
        self::loadCatalogs();

        return static::$catalogs[$iblockId]['IS_SEPARATED'];
    }


    /**
     * Возвращает ID связанного каталога продуктов
     *
     * @param int $iblockId
     *
     * @return int
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getProductIblockId(int $iblockId): int
    {
        self::loadCatalogs();

        return static::$catalogs[$iblockId]['PRODUCT_IBLOCK_ID'];
    }


    /**
     * Возвращает ID связанного каталога торговых предложений
     *
     * @param int $iblockId
     *
     * @return int
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getOfferIblockId(int $iblockId): int
    {
        self::loadCatalogs();

        return static::$catalogs[$iblockId]['OFFER_IBLOCK_ID'];
    }


    /**
     * Возвращает ID продукта, которому принадлежит переданное торговое предложение
     *
     * @param int $offerId
     * @param int $iblockId
     *
     * @return int
     */
    public static function productIdFromOfferId(int $offerId, int $iblockId = 0): int
    {
        $products = \CCatalogSku::getProductList($offerId, $iblockId);

        return (int) reset($products)['ID'];
    }


    /**
     * Загружает каталоги в локальный кэш
     *
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\LoaderException
     */
    protected static function loadCatalogs()
    {
        if (empty(static::$catalogs)) {
            Loader::includeModule('catalog');
            $catalogRes = CatalogIblockTable::getList([
                'select' => [
                    'IBLOCK_ID',
                    'PRODUCT_IBLOCK_ID',
                ],
            ]);

            while ($catalog = $catalogRes->fetch()) {
                static::$catalogs[$catalog['IBLOCK_ID']] = [
                    'ID'                => (int) $catalog['IBLOCK_ID'],
                    'PRODUCT_IBLOCK_ID' => (int) $catalog['PRODUCT_IBLOCK_ID'],
                    'OFFER_IBLOCK_ID'   => 0,
                    'IS_OFFERS'         => (int) $catalog['PRODUCT_IBLOCK_ID'] !== 0,
                    'IS_PRODUCTS'       => false,
                    'IS_SEPARATED'      => false,
                ];
            }

            foreach (static::$catalogs as $catalog) {
                if ($catalog['PRODUCT_IBLOCK_ID']) {
                    static::$catalogs[$catalog['PRODUCT_IBLOCK_ID']]['IS_PRODUCTS'] = true;
                    static::$catalogs[$catalog['PRODUCT_IBLOCK_ID']]['OFFER_IBLOCK_ID'] = $catalog['ID'];
                    static::$catalogs[$catalog['PRODUCT_IBLOCK_ID']]['IS_SEPARATED'] = true;
                    static::$catalogs[$catalog['ID']]['IS_SEPARATED'] = true;
                }
            }

        }
    }


    /**
     * Проверяет, что данный товар имеет тип "Товар с предложениями"
     *
     * @param int $productId ID товара
     *
     * @return bool
     */
    public static function isProductWithOffers(int $productId): bool
    {
        if (!isset(static::$productTypes[$productId])) {
            if ($product = \CCatalogProduct::GetByID($productId)) {
                $type = $product['TYPE'];
            } else {
                $type = null;
            }
            static::$productTypes[$productId] = $type;
        }

        return static::$productTypes[$productId] == ProductTable::TYPE_SKU;
    }


    /**
     * @return array массив всех каталогов
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getCatalogs(): array
    {
        static::loadCatalogs();
        return static::$catalogs;
    }
}
