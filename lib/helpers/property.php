<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 29.06.18
 * Time: 9:20
 */

namespace Oneway\Common\Helpers;


use Bitrix\Iblock\PropertyTable as Table;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Oneway\Logger;

final class Property
{
    /** @var Logger */
    static $logger;

    /**
     * Инициализирует класс
     *
     * @return bool
     */
    private static function init()
    {
        static::$logger = new Logger('oneway/common/errors.log');

        try {
            $res = Loader::includeModule('iblock');
            Loc::loadMessages(__FILE__);
        } catch (LoaderException $e) {
            static::$logger->critical($e->getMessage(), $e->getTrace());
            return false;
        }

        return $res;
    }

    /**
     * Добавляет свойство в инфоблок
     *
     * @param int $iblockId - ID инфоблока
     * @param string $type - Тип свойства
     * @param string $code - Код свойства
     * @param string $name - Имя свойства
     * @param bool $multiple - Множественное
     * @param bool $required - Обязательное
     * @return \Bitrix\Main\Entity\AddResult|bool
     */
    public static function addProperty(int $iblockId, string $type, string $code, string $name, bool $multiple = false, bool $required = false)
    {
        static::init();
        $result = false;

        try {
            $result = Table::add([
                'IBLOCK_ID'     => $iblockId,
                'NAME'          => $name,
                'CODE'          => $code,
                'PROPERTY_TYPE' => $type,
                'MULTIPLE'      => $multiple ? 'Y' : 'N',
                'IS_REQUIRED'   => $required ? 'Y' : 'N',
            ]);
        } catch (\Exception $e) {
            static::$logger->error($e->getMessage(), $e->getTrace());
        }

        return $result;
    }

    /**
     * Добавляет свойство в инфоблок, если его ещё не существует
     *
     * @param int $iblockId - ID инфоблока
     * @param string $type - Тип свойства
     * @param string $code - Код свойства
     * @param string $name - Имя свойства
     * @param bool $multiple - Множественное
     * @param bool $required - Обязательное
     * @return int
     * @throws ArgumentException
     */
    public static function addPropertyIfNotExists(int $iblockId, string $type, string $code, string $name, bool $multiple = false, bool $required = false)
    {
        static::init();
        try {
            $property = Table::getRow([
                'filter' => [
                    'IBLOCK_ID' => $iblockId,
                    'CODE'      => $code,
                ],
                'select' => [
                    'ID',
                    'PROPERTY_TYPE',
                    'NAME',
                ],
            ]);

            if ($property) {
                $newFields = [];
                if ($property['PROPERTY_TYPE'] !== $type) {
                    $newFields['PROPERTY_TYPE'] = $type;
                }
                if ($property['NAME'] !== $name) {
                    $newFields['NAME'] = $name;
                }

                if (!empty($newFields)) {
                    $result = Table::update($property['ID'], $newFields);

                    if (!$result || !$result->isSuccess()) {
                        if ($result) {
                            $errors = ' ' . implode(' ', $result->getErrorMessages());
                        } else {
                            $errors = '';
                        }
                        throw new ArgumentException(Loc::getMessage('OW_COMMON_UPDATE_PROPERTY_ERROR') . $errors);
                    }
                }

                return $property['ID'];
            } else {
                $result = static::addProperty($iblockId, $type, $code, $name, $multiple, $required);

                return $result->getId();
            }
        } catch (SystemException $e) {
            throw new ArgumentException(Loc::getMessage('OW_COMMON_UPDATE_PROPERTY_ERROR') . $e->getMessage());
        } catch (\Exception $e) {
            throw new ArgumentException(Loc::getMessage('OW_COMMON_UPDATE_PROPERTY_ERROR') . $e->getMessage());
        }
    }
}
