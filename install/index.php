<?
/**
 * Created by PhpStorm.
 * User: Vadim Zubko
 * Date: 26.10.17
 * Time: 10:05
 */
if (!defined('B_PROLOG_INCLUDED')) {
    if (!defined($_SERVER['DOCUMENT_ROOT'])) {
        $_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/../../../../';
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
}

use Bitrix\Main\Localization\Loc;

// @codingStandardsIgnoreStart
class oneway_common extends CModule
{
// @codingStandardsIgnoreEnd
    private static $installFiles = [
        '/js/oneway_common/bx_ajax.js',
        '/js/oneway_common/common.js',
        '/js/oneway_common/validation.js',
        '/js/oneway_common/bx_ajax.min.js',
        '/js/oneway_common/common.min.js',
        '/js/oneway_common/validation.min.js',
    ];
    private static $removeDirectories = [
        '/js/oneway_common',
    ];

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        $this->MODULE_ID = 'oneway.common';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('OW_COMMON_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('OW_COMMON_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = Loc::getMessage('OW_COMMON_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('OW_COMMON_PARTNER_URI');

        Loc::loadMessages(__FILE__);
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if ($this->isVersionD7()) {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
            \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        } else {
            $APPLICATION->ThrowException(Loc::getMessage('OW_COMMON_INSTALL_ERROR_D7'));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage('OW_COMMON_INSTALL_TITLE'), __DIR__ . '/step.php');
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

        $context = \Bitrix\Main\Context::getCurrent();
        $request = $context->getRequest();

        if ($request['step'] < 2) {
            $APPLICATION->IncludeAdminFile(Loc::getMessage('OW_COMMON_UNINSTALL_TITLE'), __DIR__ . '/unstep1.php');
        } else {
            $this->UnInstallFiles();
            $this->UnInstallEvents();
            if ($request['savedata'] !== 'Y') {
                $this->UnInstallDB();
            }

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage('OW_COMMON_UNINSTALL_TITLE'), __DIR__ . '/unstep2.php');
        }
    }

    public function InstallFiles()
    {
        foreach (self::$installFiles as $installFile) {
            CopyDirFiles(__DIR__ . $installFile, $_SERVER['DOCUMENT_ROOT'] . '/bitrix' . $installFile);
        }
    }

    public function UnInstallFiles()
    {
        foreach (self::$installFiles as $installFile) {
            unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix' . $installFile);
        }
        foreach (self::$removeDirectories as $removeDirectory) {
            rmdir($_SERVER['DOCUMENT_ROOT'] . '/bitrix' . $removeDirectory);
        }
    }

    public function InstallDB()
    {
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnProlog', $this->MODULE_ID, 'Oneway\\Common\\Options', 'checkServerHash');
    }

    public function UnInstallDB()
    {
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnProlog', $this->MODULE_ID, 'Oneway\\Common\\Options', 'checkServerHash');
    }

    protected function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.0.0');
    }

    public static function postPackageUpdate()
    {
        echo 'Обновляем файлы модуля oneway.common' . PHP_EOL;

        if (\Bitrix\Main\ModuleManager::isModuleInstalled('oneway.common')) {
            $module = CModule::CreateModuleObject('oneway.common');
            $module->InstallFiles();
        }
    }
}
