<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 12.07.18
 * Time: 18:01
 */

namespace Oneway\Common\Helpers;


use Bitrix\Catalog\PriceTable;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\LoaderException;
use Bitrix\Main\SystemException;
use Oneway\Common\Helpers\Catalog as CatalogHelper;
use Oneway\Common\Options;
use Oneway\Logger;

final class Price
{
    protected static $options;
    protected static $logger;
    protected static $useCache = true;

    protected static $prices;

    /**
     * Пересчитывает цены для всех товаров выбранного в настройках каталога
     *
     * @param int $limit
     * @param int $offset
     */
    public static function recalcAllMinMaxPrices($limit = 0, $offset = 0)
    {
        $options = self::getOptions();
        if (!$options->isCalculateMinPrice() && !$options->isCalculateMaxPrice()) {
            return;
        }

        foreach ($options->getCatalogIds() as $catalogId) {
            try {
                if (!CatalogHelper::isSeparated($catalogId)) {
                    self::$useCache = false;
                }

                $productsRes = ElementTable::getList([
                    'filter' => [
                        'IBLOCK_ID' => $catalogId,
                    ],
                    'select' => [
                        'ID',
                    ],
                    'limit'  => $limit,
                    'offset' => $offset,
                ]);
                while ($product = $productsRes->fetch()) {
                    self::recalcMinMaxPrices($product['ID'], $catalogId);
                }
            } catch (LoaderException $e) {
                self::getLogger()->error($e->getMessage(), $e->getTrace());
            } catch (SystemException $e) {
                self::getLogger()->error($e->getMessage(), $e->getTrace());
            }
        }
    }

    /**
     * Пересчитывает цены для переданного товара
     *
     * @param int $offerId Может быть как товаром с ТП, так и простым товаром
     * @param int $iblockId
     */
    public static function recalcMinMaxPrices(int $offerId, int $iblockId)
    {
        if (!isset(self::$prices[$offerId])) {
            try {
                $options = self::getOptions();
                $props = [];

                if ($options->isCalculateMinPrice()) {
                    $props['MIN_PRICE'] = self::getMinPrice($offerId, $iblockId);
                }
                if ($options->isCalculateMaxPrice()) {
                    $props['MAX_PRICE'] = self::getMaxPrice($offerId, $iblockId);
                }

                $productId = CatalogHelper::productIdFromOfferId($offerId);

                \CIBlockElement::SetPropertyValuesEx(
                    $productId ?: $offerId,
                    0,
                    $props
                );
            } catch (LoaderException $e) {
                self::getLogger()->error($e->getMessage(), $e->getTrace());
            } catch (SystemException $e) {
                self::getLogger()->error($e->getMessage(), $e->getTrace());
            }
        }
    }

    /**
     * Возвращает минимальную цену товара
     *
     * @param int $offerId Может быть как товаром с ТП, так и простым товаром
     * @param int $iblockId
     * @return int
     * @throws LoaderException
     * @throws SystemException
     */
    public static function getMinPrice($offerId, $iblockId)
    {
        $prices = static::getPrices($offerId, $iblockId);
        return min($prices);
    }

    /**
     * Возвращает максимальную цену товара
     *
     * @param int $offerId Может быть как товаром с ТП, так и простым товаром
     * @param int $iblockId
     * @return int
     * @throws LoaderException
     * @throws SystemException
     */
    public static function getMaxPrice($offerId, $iblockId)
    {
        $prices = static::getPrices($offerId, $iblockId);
        return max($prices);
    }

    /**
     * Возвращает цены товара.
     *
     * Если каталог разделён на два ИБ, то предполагаем,
     * что в настройках задан каталог с продуктами, а цены ищем для ТП
     *
     * @param int $offerId Может быть как товаром с ТП, так и простым товаром
     * @param int $iblockId
     * @return array
     * @throws LoaderException
     * @throws SystemException
     */
    protected static function getPrices($offerId, $iblockId)
    {
        if (self::$useCache && self::$prices[$offerId]) {
            $prices = self::$prices[$offerId];
        } else {
            if (CatalogHelper::isProducts($iblockId)) {
                if (CatalogHelper::isProductWithOffers($offerId)) {
                    $productId = $offerId;
                    $productIblockId = $iblockId;
                }
            } else {
                $productId = CatalogHelper::productIdFromOfferId($offerId);
                $productIblockId = CatalogHelper::getProductIblockId($iblockId);
            }

            if (isset($productId) && isset($productIblockId)) {
                // ищем цену среди всех ТП товара
                $offerIds = self::getAllOffers($productId, $productIblockId);
                $prices = self::getOfferPrices($offerIds);
            } else {
                // ТП или товар без ТП
                $prices = self::getOfferPrices($offerId);
            }
        }

        if (self::$useCache) {
            if (!empty($offerIds)) {
                foreach ($offerIds as $offerId) {
                    self::$prices[$offerId] = $prices;
                }
            } else {
                self::$prices[$offerId] = $prices;
            }
        }

        return $prices;
    }

    /**
     * Возвращает цены простого товара или торговых предложений
     *
     * @param int|array $offerId
     * @return array
     */
    protected static function getOfferPrices($offerId)
    {
        $filter = ['=PRODUCT_ID' => $offerId];

        $options = self::getOptions();
        $priceTypes = $options->getMinMaxPrices();
        if (!empty($priceTypes)) {
            $filter['=CATALOG_GROUP_ID'] = $priceTypes;
        }

        $prices = [];
        $pricesRes = PriceTable::getList([
            'filter' => $filter,
            'select' => [
                'PRICE',
                'CURRENCY',
            ],
        ]);
        while ($price = $pricesRes->fetch()) {
            $prices[] = \CCurrencyRates::ConvertCurrency($price['PRICE'], $price['CURRENCY'], CurrencyManager::getBaseCurrency());
        }

        return $prices;
    }

    /**
     * Возвращает все торговые предложения товара
     *
     * @param $productId
     * @param $iblockId
     * @return array
     */
    protected static function getAllOffers($productId, $iblockId)
    {
        $filter = ['=ACTIVE' => 'Y'];
        if (self::getOptions()->isFilterByCanBuy()) {
            $filter['CATALOG_AVAILABLE'] = 'Y';
        }
        $offers = \CCatalogSku::getOffersList(
            $productId,
            $iblockId,
            $filter
        );
        $offersIds = array_keys(reset($offers));

        return $offersIds;
    }

    protected static function getLogger()
    {
        if (empty(self::$logger)) {
            self::$logger = new Logger('/exceptions/priceHelper.log');
        }

        return self::$logger;
    }

    protected static function getOptions()
    {
        if (empty(self::$options)) {
            self::$options = new Options();
        }

        return self::$options;
    }
}
