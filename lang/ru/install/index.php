<?
$MESS['OW_COMMON_MODULE_NAME'] = 'Общая библиотека';
$MESS['OW_COMMON_MODULE_DESCRIPTION'] = 'Модуль предоставляет общие библиотеки для работы с сайтами';
$MESS['OW_COMMON_PARTNER_NAME'] = 'Oneway';
$MESS['OW_COMMON_PARTNER_URI'] = 'https://oneway.su';

$MESS['OW_COMMON_INSTALL_ERROR_D7'] = 'Для работы модуля необходима версия Битрикс с поддержкой D7';
$MESS['OW_COMMON_INSTALL_TITLE'] = 'Установка модуля Общей библиотеки';
$MESS['OW_COMMON_UNINSTALL_TITLE'] = 'Удаление модуля Общей библиотеки';
