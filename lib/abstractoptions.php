<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 02.07.18
 * Time: 14:40
 */

namespace Oneway\Common;


use Bitrix\Main\Application;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;


abstract class AbstractOptions
{
    /** @var array $errors Ошибки для показа на странице */
    protected $errors = [];

    /** @var \CAdminTabControl Объект для отрисовки страницы настроек */
    protected $tabControl;


    /**
     * Конструктор AbstractOptions.
     * Переопределите его в своём классе и обязательно выполните конструктор родителя: parent::__construct()
     */
    public function __construct()
    {
        Loc::loadMessages(Application::getDocumentRoot() . '/bitrix/modules/main/options.php');

        try {
            Loader::includeModule(static::getModuleId());
        } catch (LoaderException $e) {
            $this->addException($e);
        }
    }


    /**
     * Возвращает ID модуля
     *
     * @return string
     */
    public abstract static function getModuleId(): string;


    /**
     * Возвращает список настроек в формате:
     * ['tab' => ['optionName' => 'optionType']]
     *
     * optionType может быть 'string', 'boolean', 'array' либо свой собственный, но тогда нужно написать
     * соответствующие методы.
     *
     * @return string[]
     */
    public abstract static function getOptions(): array;


    /**
     * Возвращает список настроек для переданной вкладки
     *
     * @param string $tabId
     *
     * @return string[]
     */
    public static function getTabOptions(string $tabId): array
    {
        return static::getOptions()[$tabId];
    }


    /**
     * Возвращает список настроек без деления на вкладки
     *
     * @return array
     */
    public static function getAllOptions()
    {
        return $result = call_user_func_array('array_merge', static::getOptions());
    }


    /**
     * Возвращает список кнопок
     *
     * @return string[]
     */
    public static function getButtons(): array
    {
        return [
            [
                'name'    => 'save',
                'type'    => 'submit',
                'text'    => Loc::getMessage('MAIN_SAVE'),
                'tooltip' => Loc::getMessage('MAIN_OPT_SAVE_TITLE'),
                'class'   => 'adm-btn-save',
            ],
        ];
    }


    /**
     * Возвращает список вкладок
     *
     * @return string[]
     */
    public static function getTabs(): array
    {
        return [
            [
                'DIV'   => 'main',
                'TAB'   => Loc::getMessage('MAIN_TAB_SET'),
                'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_SET'),
            ],
        ];
    }


    /**
     * Показывает страницу настроек
     */
    public function show()
    {
        global $APPLICATION;
        $moduleAccess = $APPLICATION->GetGroupRight(static::getModuleId());
        if ($moduleAccess >= 'W') {
            $this->tabControl = new \CAdminTabControl('tabControl', static::getTabs());

            $this->showErrors();

            $this->tabControl->begin();

            echo "<form method='post' action='{$this->getFormAction()}'>";
            echo bitrix_sessid_post();

            $this->showOptions();
            $this->showButtons();
            $this->tabControl->end();

            echo '</form>';
        }
    }


    /**
     * Показывает настройки с делением на вкладки
     */
    protected function showOptions()
    {
        foreach (static::getTabs() as $tab) {
            $this->tabControl->beginNextTab();
            $options = static::getTabOptions($tab['DIV']);
            foreach ($options as $option => $type) {
                static::showOption($option, $type);
            }
        }
    }


    /**
     * Отображает одну настройку
     *
     * @param string $option
     * @param string $type
     *
     * @see AbstractOptions::showOptionValue()
     */
    protected function showOption(string $option, string $type)
    {
        if ($type === 'secret') {
            return;
        }

        $optionMessage = Loc::getMessage(static::getOptionMessage($option));
        echo "<tr><td><label for=\"{$option}\">{$optionMessage}</label></td><td>";
        $this->showOptionValue($option, $type);
        echo '</td></tr>';
    }


    /**
     * Отображает значение для настройки. Инпут, чекбокс или селект
     *
     * @param string $option
     * @param string $type
     */
    protected function showOptionValue(string $option, string $type)
    {
        switch ($type) {
            case 'boolean':
                $checked = $this->getBooleanOption($option) ? 'checked' : '';
                echo "<input type='checkbox' name='{$option}' id='{$option}' {$checked}>";
                break;
            case 'string':
                $value = $this->getStringOption($option);
                echo "<input type='text' name='{$option}' id='{$option}' value='{$value}'>";
                break;
        }
    }


    /**
     * Отображает кнопки
     */
    protected function showButtons()
    {
        $this->tabControl->buttons();
        foreach (static::getButtons() as $btn) {
            echo "<input type=\"{$btn['type']}\" name=\"{$btn['name']}\" value=\"{$btn['text']}\" title=\"{$btn['title']}\" class=\"{$btn['class']}\">";
        }
    }


    /**
     * Сохраняет настройки. Срабатывает только если передаётся POST-параметр save
     */
    public function saveOptions()
    {
        $request = Context::getCurrent()->getRequest();

        if ($this->isSaveOptions()) {
            foreach (static::getAllOptions() as $option => $type) {
                if ($type === 'secret') {
                    continue;
                }
                $value = $request->getPost($option);
                $this->saveOption($option, $value);
            }

            if (empty($this->errors)) {
                $this->showSuccess('OW_COMMON_SAVE_SETTINGS_SUCCESS');
            }
        }
    }


    /**
     * Возвращает значение настройки
     *
     * @param string            $option
     * @param array|bool|string $default
     *
     * @return array|bool|string
     */
    public function getOption(string $option, $default = false)
    {
        $type = static::getAllOptions()[$option];
        switch ($type) {
            case 'array':
                $default = $default ?: [];

                return $this->getArrayOption($option, $default);
            case 'boolean':
                return $this->getBooleanOption($option, $default);
            case 'secret':
            case 'string':
            default:
                $default = $default ?: '';

                return $this->getStringOption($option, $default);
        }
    }


    /**
     * Возвращает значение настройки типа строка
     *
     * @param string $option
     * @param string $default
     *
     * @return string
     */
    public function getStringOption(string $option, string $default = ''): string
    {
        try {
            return \Bitrix\Main\Config\Option::get(static::getModuleId(), $option, $default);
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        } catch (ArgumentNullException $e) {
            $this->addException($e);
        }

        return $default;
    }


    /**
     * Возвращает значение настройки типа boolean
     *
     * @param string $option
     * @param bool   $default
     *
     * @return bool
     */
    public function getBooleanOption(string $option, bool $default = false): bool
    {
        try {
            $bitrixDefault = $default ? 'Y' : 'N';

            return \Bitrix\Main\Config\Option::get(static::getModuleId(), $option, $bitrixDefault) === 'Y';
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        } catch (ArgumentNullException $e) {
            $this->addException($e);
        }

        return $default;
    }


    /**
     * Возвращает значение настройки типа массив
     *
     * @param string $option
     * @param array  $default
     *
     * @return array
     */
    public function getArrayOption(string $option, array $default = []): array
    {
        try {
            $serialized = \Bitrix\Main\Config\Option::get(static::getModuleId(), $option);
            if ($serialized) {
                $value = unserialize($serialized);

                return is_array($value) ? $value : $default;
            }
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        } catch (ArgumentNullException $e) {
            $this->addException($e);
        }

        return $default;
    }


    /**
     * Добавить ошибку с переданным текстом
     *
     * @param string $error
     */
    public function addError(string $error)
    {
        $this->errors[] = $error;
    }


    /**
     * Добавить ошибку с сообщением исключения
     *
     * @param \Exception $exception
     */
    public function addException(\Exception $exception)
    {
        $this->errors[] = $exception->getMessage();
    }


    /**
     * Возвращает параметр action для формы настроек
     *
     * @return string
     */
    public function getFormAction(): string
    {
        $request = Context::getCurrent()->getServer()->getRequestUri();
        $formAction = new \Bitrix\Main\Web\Uri($request);
        $formAction->addParams([
            'mid'  => urlencode(static::getModuleId()),
            'lang' => LANGUAGE_ID,
        ]);

        return $formAction->getUri();
    }


    /**
     * Проверяет, был ли передан POST-параметр save
     *
     * @return bool
     */
    protected function isSaveOptions(): bool
    {
        $request = Context::getCurrent()->getRequest();

        return $request->isPost() && !empty($request->getPost('save')) && check_bitrix_sessid();
    }


    /**
     * Сохраняет настройку в базу
     *
     * @param string            $option
     * @param array|bool|string $value
     */
    public function saveOption(string $option, $value)
    {
        $type = static::getAllOptions()[$option];
        switch ($type) {
            case 'array':
                $value = $value ?: [];
                $this->saveArrayOption($option, $value);
                break;
            case 'boolean':
                $value = $value ?: false;
                $this->saveBooleanOption($option, $value);
                break;
            case 'secret':
            case 'string':
                $value = $value ?: '';
                $this->saveStringOption($option, $value);
                break;
        }
    }


    /**
     * Сохраняет настройку типа строка
     *
     * @param string $option
     * @param string $value
     */
    protected function saveStringOption(string $option, string $value)
    {
        try {
            \Bitrix\Main\Config\Option::set(static::getModuleId(), $option, $value);
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        }
    }


    /**
     * Сохраняет настройку типа boolean
     *
     * @param string $option
     * @param bool   $value
     */
    protected function saveBooleanOption(string $option, bool $value)
    {
        try {
            $value = $value ? 'Y' : 'N';
            \Bitrix\Main\Config\Option::set(static::getModuleId(), $option, $value);
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        }
    }


    /**
     * Сохраняет настройку типа массив
     *
     * @param string $option
     * @param array  $value
     */
    protected function saveArrayOption(string $option, array $value)
    {
        try {
            $value = serialize($value);
            \Bitrix\Main\Config\Option::set(static::getModuleId(), $option, $value);
        } catch (ArgumentOutOfRangeException $e) {
            $this->addException($e);
        }
    }


    /**
     * Показывает сообщение об удачном исполнении
     *
     * @param $message
     */
    protected function showSuccess(string $message)
    {
        $adminMessage = new \CAdminMessage([
            'MESSAGE' => Loc::getMessage($message),
            'TYPE'    => 'OK',
        ]);
        echo $adminMessage->Show();
    }


    /**
     * Показывает ошибки
     */
    protected function showErrors()
    {
        if (!empty($this->errors)) {
            $errors = implode('<br>', $this->errors);
            $adminMessage = new \CAdminMessage([
                'MESSAGE' => Loc::getMessage('OW_COMMON_SAVE_SETTINGS_ERROR'),
                'TYPE'    => 'ERROR',
                'DETAILS' => $errors,
            ]);
            echo $adminMessage->Show();
        }
    }


    /**
     * Возвращает ID языковой фразы для настройки
     *
     * @param string $option
     *
     * @return string
     */
    protected static function getOptionMessage(string $option): string
    {
        static $module = false;
        if (!$module) {
            $module = strtoupper(static::getModuleId());
            $module = str_replace('.', '_', $module);
            $module = str_replace('ONEWAY', 'OW', $module);
        }

        $option = strtoupper($option);

        return "{$module}_OPTION_{$option}";
    }
}
