<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 02.07.18
 * Time: 14:24
 */

namespace Oneway\Common\Events;


use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Event;
use Oneway\Common\Helpers\Price as PriceHelper;

final class Price
{
    /** @var array elementId => iblockId */
    static $elementIblockMap = [];

    public static function onPriceAdd(Event $event)
    {
        $offerId = self::getProductIdFromEvent($event);
        $iblockId = self::getIblockIdByElementId($offerId);
        if ($iblockId) {
            PriceHelper::recalcMinMaxPrices($offerId, $iblockId);
        }
    }

    public static function onPriceUpdate(Event $event)
    {
        $offerId = self::getProductIdFromEvent($event);
        $iblockId = self::getIblockIdByElementId($offerId);
        if ($iblockId) {
            PriceHelper::recalcMinMaxPrices($offerId, $iblockId);
        }
    }

    public static function onPriceAddOld($id, $fields)
    {
        $offerId = $fields['PRODUCT_ID'];
        $iblockId = self::getIblockIdByElementId($offerId);
        if ($iblockId) {
            PriceHelper::recalcMinMaxPrices($offerId, $iblockId);
        }
    }

    public static function onPriceUpdateOld($id, $fields)
    {
        $offerId = $fields['PRODUCT_ID'];
        $iblockId = self::getIblockIdByElementId($offerId);
        if ($iblockId) {
            PriceHelper::recalcMinMaxPrices($offerId, $iblockId);
        }
    }

    protected static function getProductIdFromEvent(Event $event)
    {
        return $event->getParameter('fields')['PRODUCT_ID'];
    }

    protected static function getIblockIdByElementId($elementId)
    {
        if (!isset(self::$elementIblockMap[$elementId])) {
            $element = ElementTable::getList([
                'select' => ['IBLOCK_ID'],
                'filter' => ['ID' => $elementId]
            ])->fetch();

            if ($element) {
                self::$elementIblockMap[$elementId] = $element['IBLOCK_ID'];
            } else {
                return false;
            }
        }

        return self::$elementIblockMap[$elementId];
    }
}
