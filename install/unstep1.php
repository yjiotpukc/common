<?
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 26.10.17
 * Time: 10:57
 *
 * @global \CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid()) {
    return;
}

$uninstallMessage = new CAdminMessage(Loc::getMessage('MOD_UNINST_WARN'));
?>

<form action="<?= $APPLICATION->GetCurPage() ?>">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="hidden" name="id" value="oneway.common">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="step" value="2">
    <?= $uninstallMessage->Show() ?>
    <p><?= Loc::getMessage('MOD_UNINST_SAVE') ?></p>
    <p><input type="checkbox" name="savedata" id="savedata" value="Y" checked><label
                for="savedata"><?= Loc::getMessage('MOD_UNINST_SAVE_TABLES') ?></label></p>
    <input type="submit" name="" value="<?= Loc::getMessage('MOD_UNINST_DEL') ?>">
</form>
