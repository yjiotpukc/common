<?
$MESS['OW_COMMON_SAVE_SETTINGS_SUCCESS'] = 'Настройки сохранены';
$MESS['OW_COMMON_SAVE_SETTINGS_ERROR'] = 'Ошибка при сохранении настроек';
$MESS['OW_COMMON_RECALC_PRICES_SUCCESS'] = 'Пересчёт цен успешно завершён';
$MESS['OW_COMMON_RECALC_PRODUCTS_SALES_SUCCESS'] = 'Пересчёт продаж успешно завершён';

$MESS['OW_COMMON_OPTION_CATALOG_ID'] = 'Инфобок каталога';
$MESS['OW_COMMON_OPTION_CALCULATE_MIN_PRICE'] = 'Рассчитывать минимальную цену';
$MESS['OW_COMMON_OPTION_CALCULATE_MAX_PRICE'] = 'Рассчитывать максимальную цену';
$MESS['OW_COMMON_OPTION_CALCULATE_PRODUCTS_SALES'] = 'Рассчитывать количество продаж товара';
$MESS['OW_COMMON_OPTION_CALCULATE_PRODUCTS_SALES_PERIOD'] = 'Период рассчёта (в днях)';
$MESS['OW_COMMON_OPTION_FILTER_BY_CAN_BUY'] = 'Фильтровать по доступности';
$MESS['OW_COMMON_OPTION_MIN_MAX_PRICES'] = 'Учитывать только эти типы цен';
$MESS['OW_COMMON_OPTION_BUTTON_RECALC_PRICES'] = 'Пересчитать цены';
$MESS['OW_COMMON_OPTION_BUTTON_RECALC_PRICES_TITLE'] = 'Пересчитать минимальную и максимальную цены у товаров';
$MESS['OW_COMMON_OPTION_BUTTON_RECALC_PRODUCTS_SALES'] = 'Пересчитать продажи';
$MESS['OW_COMMON_OPTION_BUTTON_RECALC_PRODUCTS_SALES_TITLE'] = 'Пересчитать количество продаж товаров';
