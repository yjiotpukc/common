OW = {
    loaders: {},
    config: {
        loader: {
            withBorders: true,
            defaultNode: BX('page'),
            overlayIdPostfix: '-overlay',
            style: {
                zIndex: 1100,
                backgroundColor: 'white',
                position: 'absolute',
                opacity: 0.8,
                display: 'block'
            },
            imagePath: '/local/images/loader.gif',
            imageStyle: {
                top: '50%',
                left: '50%',
                marginLeft: '-20px',
                marginTop: '-20px',
                position: 'absolute',
                zIndex: 1150
            }
        },
        popupIdPostfix: '_popup',
        stickyHeaderHeight: 0
    }
};

/**
 * Аналог BX.namespace.
 * @param {string} namespace
 * @returns {{}}
 * @see BX.namespace
 */
OW.namespace = function (namespace) {
    var parts = namespace.split('.');
    var parent = OW;

    if (parts[0] === 'OW') {
        parts = parts.slice(1);
    }

    for (var i = 0; i < parts.length; i++) {
        if (typeof parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }

    return parent;
};

/**
 * Выполняет переданную функцию для каждого элемента из переданного массива.
 * Отличается от Array.forEach() тем, что работает ещё и с объектами и NodeList в IE.
 * @param {{}|[]} iterable
 * @param {function} func
 * @param {{}} [context=window]
 */
OW.forEach = function (iterable, func, context) {
    if (!context) {
        context = window;
    }
    func = func.bind(context);

    if (iterable instanceof Array) {
        iterable.forEach(func);
    } else {
        for (var inner in iterable) {
            if (iterable.hasOwnProperty(inner)) {
                func(iterable[inner], inner, iterable);
            }
        }
    }
};

/** На каждый элемент переданного массива вешается переданный обработчик события.
 * @param {Element[]} elements
 * @param {string} event
 * @param {function} func
 * @param {{}} [context=window]
 */
OW.forEachBind = function (elements, event, func, context) {
    if (!context) {
        context = window;
    }
    func = func.bind(context);
    var boundBind = BX.bind.bind(window, item, event, func);

    elements.forEach(boundBind);
};

/**
 * Возвращает переданное значение, если оно не равно NaN и 0 в противном случае.
 * @param {*} value
 * @returns {number}
 */
OW.nanToZero = function (value) {
    return isNaN(value) ? 0 : value;
};

/**
 * Возвращает размеры и положение элемента.
 * @param {Element} nodeFrom Если не передано, берётся OW.config.loader.defaultNode
 * @param {boolean} [withBorders] Если не boolean, то берётся OW.config.loader.withBorders
 * @returns {{width: number, height:number, top:number, left:number}}
 * {
 * &emsp; width - ширина,
 * &emsp; height - высота
 * &emsp; top - расстояние от верхнего края
 * &emsp; left - расстояние от левого края
 * }
 */
OW.getNodeRect = function (nodeFrom, withBorders) {
    if (!nodeFrom) {
        nodeFrom = OW.config.loader.defaultNode;
    }
    if (typeof withBorders !== 'boolean') {
        withBorders = OW.config.loader.withBorders;
    }

    var dimensions = {};
    var rect = nodeFrom.getBoundingClientRect();
    var borders = {
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    };

    if (withBorders) {
        // IE 11 выдаёт ширину бордера 'medium'
        borders = {
            top: OW.nanToZero(parseInt(BX.style(nodeFrom, 'border-top-width'))),
            bottom: OW.nanToZero(parseInt(BX.style(nodeFrom, 'border-bottom-width'))),
            right: OW.nanToZero(parseInt(BX.style(nodeFrom, 'border-right-width'))),
            left: OW.nanToZero(parseInt(BX.style(nodeFrom, 'border-left-width')))
        };
    }

    // В IE у document.body нулевые scrollTop и scrollLeft
    var scrollTop = document.documentElement
        ? document.documentElement.scrollTop
        : document.body.scrollTop;
    var scrollLeft = document.documentElement
        ? document.documentElement.scrollLeft
        : document.body.scrollLeft;

    if (!rect.width) {
        rect.width = rect.right - rect.left;
    }
    if (!rect.height) {
        rect.height = rect.bottom - rect.top;
    }

    dimensions.width = rect.width - (borders.left + borders.right);
    dimensions.height = rect.height - (borders.top + borders.bottom);
    dimensions.top = rect.top + scrollTop + borders.top;
    dimensions.left = rect.left + scrollLeft + borders.left;

    return dimensions;
};

/**
 * Возвращает размеры и положение элемента с указанием единиц измерения px.
 * @param nodeFrom
 * @param {boolean} [withBorders]
 * @returns {{width: string, height:string, top:string, left:string}}
 * {
 * &emsp; width - ширина,
 * &emsp; height - высота
 * &emsp; top - расстояние от верхнего края
 * &emsp; left - расстояние от левого края
 * }
 */
OW.getNodeRectInPixels = function (nodeFrom, withBorders) {
    var dimensions = OW.getNodeRect(nodeFrom, withBorders);
    return {
        width: dimensions.width + 'px',
        height: dimensions.height + 'px',
        top: dimensions.top + 'px',
        left: dimensions.left + 'px'
    };
};


/**
 * Отображает прелоадер.
 * @param id
 * @param node
 * @returns {number}
 */
OW.showLoader = function (id, node) {
    var style = BX.mergeEx({}, OW.config.loader.style, OW.getNodeRectInPixels(node));

    if (OW.loaders[id]) {
        BX.adjust(OW.loaders[id], {
            style: style
        })
    } else {
        var loader = BX.create('div', {
            props: {
                id: id + OW.config.loader.overlayIdPostfix
            },
            style: style
        });
        OW.loaders[id] = loader;

        var image = BX.create('IMG', {
            props: {src: OW.config.loader.imagePath},
            style: OW.config.loader.imageStyle
        });
        BX.append(image, loader);
        BX.append(loader, document.body);
    }
};

/**
 * Скрывает прелоадер.
 * @param id
 */
OW.hideLoader = function (id) {
    var loader = OW.loaders[id];
    if (loader) {
        BX.adjust(loader, {
            style: {display: 'none'}
        });
    }
};

/**
 * Возвращает объект с параметрами и значениями, переданными в URL.
 * @return {{show_popup: string|boolean}}
 */
OW.parseRequest = function () {
    var queryParams = {
        show_popup: false
    };
    var requestStr = document.location.href.split('?')[1];
    if (!requestStr) {
        return queryParams;
    }

    var pairsStr = requestStr.split('&');
    return pairsStr.reduce(function (queryParamsObj, pair) {
        var pairAr = pair.split('=');
        queryParamsObj[pairAr[0]] = pairAr[1];
        return queryParamsObj;
    }, queryParams);
};

/**
 * Проверяет URL страницы на наличие нужных параметров.
 * Сейчас проверяет только show_popup.
 */
OW.checkUrl = function () {
    var request = OW.parseRequest();
    if (request.show_popup) {
        OW.showPopup(request.show_popup);
    }
};

/**
 * Проверяет куки на наличие нужных параметров.
 * Сейчас проверяет только show_popup.
 */
OW.checkCookies = function () {
    var popupId = BX.getCookie('show_popup');
    if (popupId) {
        BX.setCookie('show_popup', '', {expires: 0});
        OW.showPopup(popupId);
    }
};

/**
 * Отображает popup.
 * @param popup
 */
OW.showPopup = function (popup) {
    if (popup) {
        BX.style(BX(popup + OW.config.popupIdPostfix), 'display', 'block');
    }
};

/**
 * Скрывает popup.
 * @param popup
 */
OW.closePopup = function (popup) {
    if (popup) {
        BX.style(BX(popup + OW.config.popupIdPostfix), 'display', 'none');
        OW.hideLoader(popup);
    }
};
