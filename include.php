<?php

CJSCore::RegisterExt('ow_common', [
    'js' => '/bitrix/js/oneway_common/common.js',
    'skip_core' => false,
]);

CJSCore::RegisterExt('ow_bx_ajax', [
    'js' => '/bitrix/js/oneway_common/bx_ajax.js',
    'rel' => [
        'ajax',
        'ow_common',
    ],
]);

CJSCore::RegisterExt('ow_validation', [
    'js' => '/bitrix/js/oneway_common/validation.js',
    'rel' => ['ow_common']
]);

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
