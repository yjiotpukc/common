<?
$MESS['OW_COMMON_MODULE_NAME'] = 'Common lib';
$MESS['OW_COMMON_MODULE_DESCRIPTION'] = 'Module provides common libs for sites';
$MESS['OW_COMMON_PARTNER_NAME'] = 'Oneway';
$MESS['OW_COMMON_PARTNER_URI'] = 'https://oneway.su';

$MESS['OW_COMMON_INSTALL_ERROR_D7'] = 'Module needs Bitrix with D7 support';
$MESS['OW_COMMON_INSTALL_TITLE'] = 'Installing module Common lib';
$MESS['OW_COMMON_UNINSTALL_TITLE'] = 'Uninstalling module Common lib';
