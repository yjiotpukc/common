OW.namespace('validation');

OW.config.validation = {
    regex: {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Zа-яА-ЯёЁ\-0-9]+\.)+[a-zA-Zа-яА-ЯёЁ]{2,}))$/,
        phone: /^\+7 \(\d\d\d\) \d\d\d-\d\d-\d\d$/
    },
    inputWrapperClass: null,
    inputWrapperErrorClass: null,
    inputWrapperSuccessClass: null,
    inputWrapperErrorInnerTextClass: null
};

/**
 * Валидация Email.
 * @param {string} email
 * @returns {boolean}
 */
OW.validation.validateEmail = function (email) {
    return OW.config.validation.regex.email.test(email);
};

/**
 * Валидация телефона.
 * @param {string} phone
 * @returns {boolean}
 */
OW.validation.validatePhone = function (phone) {
    return OW.config.validation.regex.phone.test(phone);
};

/**
 * Если инпут не в контейнере с классом inputWrapper, то оборачивает его.
 * @param {HTMLInputElement|HTMLTextAreaElement} input
 */
OW.validation.wrapInputWithValidator = function (input) {
    if (OW.config.validation.inputWrapperClass) {
        if (!BX.hasClass(input.parentNode, OW.config.validation.inputWrapperClass)) {
            var wrapper = BX.create('div', {attrs: {class: OW.config.validation.inputWrapperClass}});
            input.parentNode.appendChild(wrapper);
            wrapper.appendChild(input);
        }
    }
};

/**
 * Добавляет DOM-элемент для текста ошибки, если его нет, к контейнеру с классом inputWrapper для переданного инпута.
 * @param {HTMLInputElement|HTMLTextAreaElement} input
 */
OW.validation.addErrorTextToInputWrapper = function (input) {
    if (OW.config.validation.inputWrapperClass && OW.config.validation.inputWrapperErrorInnerTextClass) {
        if (!BX.findChild(input.parentNode, {class: OW.config.validation.inputWrapperErrorInnerTextClass})) {
            BX.append(
                BX.create('div', {attrs: {class: OW.config.validation.inputWrapperErrorInnerTextClass}}),
                input.parentNode
            );
        }
    }
};

/**
 * Делает контейнер инпута ошибочным. Красный цвет и крестик, например.
 * Также пишет текст ошибки.
 * @param {HTMLInputElement|HTMLTextAreaElement} input
 * @param {string} text
 */
OW.validation.inputError = function (input, text) {
    OW.validation.wrapInputWithValidator(input);
    if (OW.config.validation.inputWrapperSuccessClass) {
        BX.removeClass(input.parentNode, OW.config.validation.inputWrapperSuccessClass);
    }
    if (OW.config.validation.inputWrapperErrorClass) {
        BX.addClass(input.parentNode, OW.config.validation.inputWrapperErrorClass);
    }
    OW.validation.addErrorTextToInputWrapper(input);
    if (OW.config.validation.inputWrapperErrorInnerTextClass) {
        BX.findChild(input.parentNode, {class: OW.config.validation.inputWrapperErrorInnerTextClass}).innerHTML = text;
    }
};

/**
 * Делает контейнер инпута правильным. Зелёный цвет и галочка, например.
 * @param {HTMLInputElement|HTMLTextAreaElement} input
 */
OW.validation.inputSuccess = function (input) {
    OW.validation.wrapInputWithValidator(input);
    if (OW.config.validation.inputWrapperErrorClass) {
        BX.removeClass(input.parentNode, OW.config.validation.inputWrapperErrorClass);
    }
    if (OW.config.validation.inputWrapperSuccessClass) {
        BX.addClass(input.parentNode, OW.config.validation.inputWrapperSuccessClass);
    }
};

/**
 * Делает контейнер инпута правильным только в том случае, если до этого он был ошибочным.
 * @param {HTMLInputElement|HTMLTextAreaElement} input
 */
OW.validation.inputSuccessIfNeeded = function (input) {
    if (OW.config.validation.inputWrapperClass) {
        if (BX.hasClass(input.parentNode, OW.config.validation.inputWrapperClass)) {
            OW.validation.inputSuccess(input);
        }
    }
};

/**
 * Обработчик события submit для валидации форм.
 * @param event
 */
OW.validation.formSubmitTester = function (event) {
    if (!OW.validation.testForm(event.target)) {
        event.preventDefault();
    }
};

/**
 * Проверяет все поля формы на правильность заполнения.
 * @param {HTMLFormElement} form
 * @returns {boolean}
 */
OW.validation.testForm = function (form) {
    /**
     * @type {HTMLInputElement|HTMLTextAreaElement|boolean}
     */
    var firstInvalid = false;
    var inputs = BX.findChildren(form, {tag: /^(input|textarea)$/i}, true);

    inputs.forEach(function (input) {
        if (input.type !== 'hidden' &&
            !input.disabled &&
            (!OW.validation.testInputRequired(input) ||
                !OW.validation.testInputEmail(input) ||
                !OW.validation.testInputConfirmPass(input) ||
                !OW.validation.testInputPhone(input)) &&
            !firstInvalid
        ) {
            firstInvalid = input;
        }
    });

    if (typeof firstInvalid !== 'boolean' && !BX.findParent(firstInvalid, {class: 'popup'})) {
        var firstInvalidTop = OW.getNodeRect(firstInvalid).top;
        // Компенсируем хедер
        window.scrollTo(0, firstInvalidTop - OW.config.stickyHeaderHeight);
        firstInvalid.focus();
    }

    return !firstInvalid;
};

/**
 * Возвращает новую функцию, которая проверяет переданный в неё инпут на правильность заполнения и делает его контейнер ошибочным либо правильным.
 * @param {function(HTMLInputElement|HTMLTextAreaElement): boolean} predicate Функция проверки инпута
 * @param {string} text Текст ошибки
 * @returns {function(HTMLInputElement|HTMLTextAreaElement): boolean}
 */
OW.validation.testInput = function (predicate, text) {
    return function (input) {
        if (!predicate(input)) {
            OW.validation.inputError(input, text);
        } else {
            OW.validation.inputSuccessIfNeeded(input);
        }
        return predicate(input);
    }
};

/**
 * Проверяет обязательные инпуты в форме.
 * @type {function((HTMLInputElement|HTMLTextAreaElement)): boolean}
 */
OW.validation.testInputRequired = OW.testInput(function (input) {
    return !input.required || input.value.trim() !== '';
}, 'Поле не заполнено');

/**
 * Проверяет переданный инпут на соответствие регулярному выражению для Email-адреса.
 * У инпута должен быть type=email.
 * @type {function((HTMLInputElement|HTMLTextAreaElement)): boolean}
 */
OW.validation.testInputEmail = OW.testInput(function (input) {
    return input.type !== 'email' ||
        (!input.required && input.value === '') ||
        OW.validation.validateEmail(input.value)
}, 'Невалидный Email');

/**
 * Проверяет переданный инпут на соответствие регулярному выражению для телефона.
 * У инпута должен быть класс phone-input.
 * @type {function((HTMLInputElement|HTMLTextAreaElement)): boolean}
 */
OW.validation.testInputPhone = OW.testInput(function (input) {
    return !BX.hasClass(input, 'phone-input') || OW.validation.validatePhone(input.value);
}, 'Поле не заполнено');

/**
 * Проверяет поле подтверждения пароля на соответствие с полем пароля.
 * У инпута должен быть name=CONFIRM_PASSWORD.
 * @type {function((HTMLInputElement|HTMLTextAreaElement)): boolean}
 */
OW.validation.testInputConfirmPass = OW.testInput(function (input) {
    if (/CONFIRM_PASSWORD/.test(input.name)) {
        var password,
            confirmPassword,
            passwordInputs = BX.findChildren(input.form, {
                tag: 'input',
                props: {
                    type: 'password'
                }
            }, true);
        passwordInputs.forEach(function (formInput) {
            if (/CONFIRM_PASSWORD/.test(formInput.name)) {
                confirmPassword = formInput.value;
            } else {
                password = formInput.value;
            }
        });

        if (password && confirmPassword) {
            return password === confirmPassword;
        } else {
            return true;
        }
    } else {
        return true;
    }
}, 'Пароли не совпадают');
