<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 28.06.18
 * Time: 16:05
 */

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Oneway\Common\Options;

if (!defined('B_PROLOG_INCLUDED') || (B_PROLOG_INCLUDED !== true)) {
    die();
}

try {
    Loader::includeModule($mid);

    $options = new Options();
    $options->saveOptions();
    $options->show();
} catch (LoaderException $e) {
    $adminMessage = new \CAdminMessage([
        'MESSAGE' => $e->getMessage(),
        'TYPE'    => 'ERROR',
    ]);
    echo $adminMessage->Show();
}
