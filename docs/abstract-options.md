# Абстрактный класс настроек модуля

Чтобы облегчить задачу создания нового модуля, был написан класс `Oneway\Common\AbstractOptions`.
Также написана консольная [утилита](http://gitlab.owstudio.ru/solutions/console), для создания заготовки модуля.

## Как с этим работать?

Для начала создайте класс для настроек конкретного модуля.
Он должен наследоваться от абстрактного класса настроек.
Реализуйте метод `getModuleId`. Он должен возвращать ID модуля.

Итак, у настроек модуля есть три "сущности". Кнопка, Вкладка и Настройка.

### Кнопки

По умолчанию есть только одна кнопка для сохранения настроек.
Как добавить свои?
Вам нужно переопределить метод `getButtons`.
Например, так:

```php
public static function getButtons(): array
{
    return array_merge(parent::getButtons(), [
        [
            'name'    => 'recalc_prices',
            'type'    => 'submit',
            'text'    => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRICES'),
            'tooltip' => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRICES_TITLE'),
        ],
    ]);
}
```

Здесь мы добавляем кнопку для пересчёта цен.

### Вкладки

По умолчанию есть только вкладка Настройки.
Для добавления своих переопределите метод `getTabs`.

### Настройки

Для добавления своих настроек реализуйте метод `getOptions`.
Возвращать он должен массив вида

```php
[
    'main' => [
        'option_1' => 'string',
        'option_2' => 'boolean',
        'option_3' => 'boolean',
        'option_4' => 'array',
    ],
    'additional' => [
        'option_4' => 'string',
        'option_5' => 'string',
    ]
]
```

Первый уровень - это ID вкладки.
На втором уровне в ключе хранится название настройки, то есть, как она будет храниться в БД.
В значении хранится тип настройки.
Он используется для способа сохранения настройки.
Как известно, в базе данных все настройки хранятся в строковом виде.
Значит, нам нужно привести значение к строковому виду.
Строка (string) сохранится и так.
Булев тип (boolean) переводится в Y или N.
Массив (array) просто сериализуется.

А где же текст настройки?
Текст настройки берётся из языковой фразы.
Файл с ними должен подключаться в конструкторе класса.
ID языковой фразы настройки должен быть в верхнем регистре и строиться вот так: `<VENDOR>_<MODULE_ID>_OPTION_<OPTION_NAME>`.
Например, для настройки `catalog_id` модуля `oneway.common`: `OW_COMMON_OPTION_CATALOG_ID`.
`ONEWAY` сокращается до `OW`, чтобы хоть как-то сократить невероятно длинный ID фразы.

## Продвинутая информация

Вы можете переопределить метод `saveOption`, чтобы как-то по-особому сохранить настройку.
Например, создать свойство инфоблока.

Если ваша настройка должна отображаться как-то особенно, переопределите метод `showOptionValue`.
Например, так:

```php
protected function showOptionValue(string $option, string $type)
{
    switch ($option) {
        case 'catalog_id':
            echo \GetIBlockDropDownList($this->getCatalogId(), 'catalog_iblock_type', 'catalog_id');
            break;
        default:
            parent::showOptionValue($option, $type);
    }
}
```
