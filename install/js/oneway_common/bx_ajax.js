OW.namespace('OW.ajax');

/**
 * Инициализация механизма, который "чинит" стандартный битровый аякс в компонентах.
 * При возврате на страницу через навигацию браузера, то есть, кнопки "Назад" и "Вперёд" все скрипты, у которых указан класс "eval_on_return" выполнятся повторно.
 * Например, все регистрации обработчиков событий.
 */
OW.ajax.init = function () {
    // Добавляем события BXAjaxStart и BXAjaxEnd в BX.ajax
    if (!!BX.ajax) {
        var trueBXAjax = BX.ajax;

        BX.ajax = function () {
            BX.onCustomEvent('BXAjaxStart');
            var xhr = trueBXAjax.apply(this, arguments);

            var trueHandler = xhr.onreadystatechange;
            xhr.onreadystatechange = function () {
                if (this.readyState === 4) {    // AJAX завершён
                    BX.onCustomEvent('BXAjaxEnd', [this]);
                }
                trueHandler.apply(this, arguments);
            };

            return xhr;
        };

        for (var prop in trueBXAjax) {
            // Копируем все поля оригинального объекта
            // noinspection JSUnfilteredForInLoop
            BX.ajax[prop] = trueBXAjax[prop];
        }
    }

    // Сохраняем адрес страницы перед аякс запросом
    BX.addCustomEvent('BXAjaxStart', function () {
        OW.ajax.lastPageUrl = document.location.pathname + document.location.search;
    });

    // Парсим ответ и устанавливаем скрипты, которые нужно запустить при возврате на страницу
    // То есть, при использовании кнопок "Назад" и "Вперёд" в браузере все нужные скрипты запустятся сами
    BX.addCustomEvent('BXAjaxEnd', function (xhr) {
        var element = document.createElement('div');
        element.innerHTML = xhr.responseText;

        OW.ajax.setScriptsOnReturn(element);
    });

    // Сохраняем в историю браузера скрипты, которые нужно исполнить при возврате на страницу
    BX.addCustomEvent('onComponentAjaxHistoryGetState', function (state) {
        state.scriptsOnReturn = OW.ajax.scriptsOnReturn;
    });

    // Исполняем скрипты при возврате на страницу
    // В теории небезопасно, но я не могу представить, как сюда может попасть вредоносный код
    BX.addCustomEvent('onComponentAjaxHistorySetState', function (state) {
        if (state.scriptsOnReturn && state.scriptsOnReturn instanceof Array) {
            state.scriptsOnReturn.forEach(eval);
        }
    });

    BX.ready(OW.ajax.setScriptsOnReturn.bind(window, document));
};

/**
 * Берёт DOM-элемент, ищет все <script> с классом "eval_on_return" и записывает их в OW.ajax.scriptsOnReturn.
 * @param element
 */
OW.ajax.setScriptsOnReturn = function (element) {
    var scriptsStr = [];

    var scripts = element.querySelectorAll('script.eval_on_return');

    if (scripts && scripts.length > 0) {
        OW.forEach(scripts, function(script) {
            scriptsStr.push(script.innerHTML.trim());
        });
    }

    OW.ajax.scriptsOnReturn = scriptsStr;
};
