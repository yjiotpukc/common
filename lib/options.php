<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 02.07.18
 * Time: 14:40
 */

namespace Oneway\Common;


use Bitrix\Catalog\GroupTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Context;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\SystemException;
use Oneway\Common\Events\Price as PriceEvents;
use Oneway\Common\Helpers\Catalog as CatalogHelper;
use Oneway\Common\Helpers\Price as PriceHelper;
use Oneway\Common\Helpers\ProductSalesCounter;
use Oneway\Common\Helpers\Property as PropertyHelper;
use Oneway\Events\IBlock;


final class Options extends AbstractOptions
{
    const EVENT_SERVER_CHANGED = 'OnServerChanged';

    const OPTION_CATALOG_ID = 'catalog_id';
    const OPTION_CALCULATE_MIN_PRICE = 'calculate_min_price';
    const OPTION_CALCULATE_MAX_PRICE = 'calculate_max_price';
    const OPTION_FILTER_BY_CAN_BUY = 'filter_by_can_buy';
    const OPTION_MIN_MAX_PRICES = 'min_max_prices';
    const OPTION_SERVER_HASH = 'server_hash';
    const OPTION_CALCULATE_PRODUCTS_SALES = 'calculate_products_sales';
    const OPTION_CALCULATE_PRODUCTS_SALES_PERIOD = 'calculate_products_sales_period';


    public function __construct()
    {
        parent::__construct();
        Loc::loadMessages(__FILE__);

        try {
            Loader::includeModule('iblock');
            Loader::includeModule('sale');
        } catch (LoaderException $e) {
            $this->addException($e);
        }
    }


    /** @inheritdoc */
    public static function getModuleId(): string
    {
        return 'oneway.common';
    }


    /** @inheritdoc */
    public static function getOptions(): array
    {
        return [
            'main' => [
                self::OPTION_CATALOG_ID                      => 'array',
                self::OPTION_CALCULATE_MIN_PRICE             => 'boolean',
                self::OPTION_CALCULATE_MAX_PRICE             => 'boolean',
                self::OPTION_FILTER_BY_CAN_BUY               => 'boolean',
                self::OPTION_MIN_MAX_PRICES                  => 'array',
                self::OPTION_CALCULATE_PRODUCTS_SALES        => 'boolean',
                self::OPTION_CALCULATE_PRODUCTS_SALES_PERIOD => 'string',
                self::OPTION_SERVER_HASH                     => 'secret',
            ],
        ];
    }


    /** @inheritdoc */
    public static function getButtons(): array
    {
        return array_merge(parent::getButtons(), [
            [
                'name'    => 'recalc_prices',
                'type'    => 'submit',
                'text'    => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRICES'),
                'tooltip' => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRICES_TITLE'),
            ],
            [
                'name'    => 'recalc_products_sales',
                'type'    => 'submit',
                'text'    => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRODUCTS_SALES'),
                'tooltip' => Loc::getMessage('OW_COMMON_OPTION_BUTTON_RECALC_PRODUCTS_SALES_TITLE'),
            ],
        ]);
    }


    public function getBooleanOption(string $option, bool $default = false): bool
    {
        if ($option === self::OPTION_SERVER_HASH) {
            $default = true;
        }

        return parent::getBooleanOption($option, $default);
    }


    /** @inheritdoc */
    protected function showOptionValue(string $option, string $type)
    {
        switch ($option) {
            case self::OPTION_CATALOG_ID:
                echo $this->getCatalogsHtml();
                break;
            case self::OPTION_MIN_MAX_PRICES:
                echo $this->getPriceTypesHtml();
                break;
            default:
                parent::showOptionValue($option, $type);
        }
    }


    /**
     * Проверяет, был ли передан POST-параметр recalc_prices
     *
     * @return bool
     */
    protected function isRecalcPrices(): bool
    {
        $request = Context::getCurrent()->getRequest();

        return $request->isPost() && !empty($request->getPost('recalc_prices')) && check_bitrix_sessid();
    }


    /**
     * Проверяет, был ли передан POST-параметр recalc_products_sales
     *
     * @return bool
     */
    protected function isRecalcProductsSales(): bool
    {
        $request = Context::getCurrent()->getRequest();

        return $request->isPost() && !empty($request->getPost('recalc_products_sales')) && check_bitrix_sessid();
    }


    /** @inheritdoc */
    public function saveOptions()
    {
        parent::saveOptions();

        if ($this->isSaveOptions()) {
            if (!$this->isCalculateMinPrice() && !$this->isCalculateMaxPrice()) {
                $this->unregisterEvent('price_change');
            } else {
                $this->registerEvent('price_change');
            }
        } elseif ($this->isRecalcPrices()) {
            PriceHelper::recalcAllMinMaxPrices();
            $this->showSuccess('OW_COMMON_RECALC_PRICES_SUCCESS');
        } elseif ($this->isRecalcProductsSales()) {
            ProductSalesCounter::fullRecalc();
            $this->showSuccess('OW_COMMON_RECALC_PRODUCTS_SALES_SUCCESS');
        }
    }


    /** @inheritdoc */
    protected function saveBooleanOption(string $option, bool $value)
    {
        switch ($option) {
            case self::OPTION_CALCULATE_MIN_PRICE:
                $this->saveCalculateMinPrice($value);
                break;
            case self::OPTION_CALCULATE_MAX_PRICE:
                $this->saveCalculateMaxPrice($value);
                break;
            case self::OPTION_CALCULATE_PRODUCTS_SALES:
                $this->saveCalculateProductsSales($value);
                break;
        }

        parent::saveBooleanOption($option, $value);
    }


    /**
     * Возвращает ID каталогов, выбранных в настройках
     *
     * @return array
     */
    public function getCatalogIds(): array
    {
        static $catalogIds;

        if (!$catalogIds) {
            $request = Context::getCurrent()->getRequest();
            if ($request->getPost(self::OPTION_CATALOG_ID)) {
                $catalogIds = $request->getPost(self::OPTION_CATALOG_ID);
            } else {
                $catalogIds = $this->getArrayOption(self::OPTION_CATALOG_ID);
            }
        }

        return $catalogIds;
    }


    /**
     * Нужно ли считать минимальную цену
     *
     * @return bool
     */
    public function isCalculateMinPrice(): bool
    {
        return $this->getBooleanOption(self::OPTION_CALCULATE_MIN_PRICE);
    }


    /**
     * Нужно ли считать максимальную цену
     *
     * @return bool
     */
    public function isCalculateMaxPrice(): bool
    {
        return $this->getBooleanOption(self::OPTION_CALCULATE_MAX_PRICE);
    }


    /**
     * Фильтровать по доступности товара
     *
     * @return bool
     */
    public function isFilterByCanBuy(): bool
    {
        return $this->getBooleanOption(self::OPTION_FILTER_BY_CAN_BUY);
    }


    /**
     * Возвращает массив с ценами, которые участвуют в расчёте
     *
     * @return array
     */
    public function getMinMaxPrices(): array
    {
        return $this->getArrayOption(self::OPTION_MIN_MAX_PRICES);
    }


    /**
     * Сохраняет настройку "Считать минимальную цену" и создаёт свойство MIN_PRICE, если его нет
     *
     * @param bool $value
     */
    public function saveCalculateMinPrice(bool $value)
    {
        if ($value) {
            try {
                foreach ($this->getCatalogIds() as $catalogId) {
                    PropertyHelper::addPropertyIfNotExists($catalogId, \Bitrix\Iblock\PropertyTable::TYPE_NUMBER, 'MIN_PRICE', 'Минимальная цена');
                }
            } catch (ArgumentException $e) {
                $this->addException($e);
            }

            EventManager::getInstance();
        }
    }


    /**
     * Сохраняет настройку "Считать максимальную цену" и создаёт свойство MAX_PRICE, если его нет
     *
     * @param bool $value
     */
    public function saveCalculateMaxPrice(bool $value)
    {
        if ($value) {
            try {
                foreach ($this->getCatalogIds() as $catalogId) {
                    PropertyHelper::addPropertyIfNotExists($catalogId, \Bitrix\Iblock\PropertyTable::TYPE_NUMBER, 'MAX_PRICE', 'Максимальная цена');
                }
            } catch (ArgumentException $e) {
                $this->addException($e);
            }
        }
    }


    /**
     * Сохраняет настройку "Считать количество продаж" и создаёт свойство SALES_COUNT, если его нет
     *
     * @param bool $value
     */
    public function saveCalculateProductsSales(bool $value)
    {
        if ($value) {
            try {
                foreach ($this->getCatalogIds() as $catalogId) {
                    if (CatalogHelper::isSeparated($catalogId)) {
                        if (CatalogHelper::isProducts($catalogId)) {
                            $productsIblockId = $catalogId;
                            $skusIblockId = CatalogHelper::getOfferIblockId($catalogId);
                        } else {
                            $skusIblockId = $catalogId;
                            $productsIblockId = CatalogHelper::getProductIblockId($catalogId);
                        }

                        PropertyHelper::addPropertyIfNotExists($productsIblockId, \Bitrix\Iblock\PropertyTable::TYPE_NUMBER, 'SALES_COUNT', 'Количество продаж');
                        PropertyHelper::addPropertyIfNotExists($skusIblockId, \Bitrix\Iblock\PropertyTable::TYPE_NUMBER, 'SALES_COUNT', 'Количество продаж');
                    } else {
                        PropertyHelper::addPropertyIfNotExists($catalogId, \Bitrix\Iblock\PropertyTable::TYPE_NUMBER, 'SALES_COUNT', 'Количество продаж');
                    }
                }

            } catch (ArgumentException $e) {
                $this->addException($e);
            }
        }
    }


    /**
     * Возвращает вёрстку выбора каталогов
     *
     * @return string
     */
    public function getCatalogsHtml(): string
    {
        $catalogs = $this->getCatalogs();

        // сортируем каталоги сначала по типу инфоблока, затем по названию инфоблока
        usort($catalogs, function($a, $b) {
            if ($a['IBLOCK_TYPE_ID'] === $b['IBLOCK_TYPE_ID']) {
                return $a['ID'] > $b['ID'];
            }
            return $a['IBLOCK_TYPE_ID'] > $b['IBLOCK_TYPE_ID'];
        });

        $selectedCatalogs = $this->getCatalogIds();

        $html = '';
        foreach ($catalogs as $catalog) {
            $catalogId = $catalog['ID'];
            $catalogName = $catalog['NAME'];
            $catalogIBlockTypeName = $catalog['IBLOCK_TYPE_NAME'];
            $checked = in_array($catalogId, $selectedCatalogs) ? 'checked' : '';
            $label = "{$catalogId}: {$catalogIBlockTypeName} - {$catalogName}";
            $optionName = static::OPTION_CATALOG_ID;

            $html .= <<<HTML
                <div style="padding-bottom: 10px;">
                    <input type='checkbox' 
                           name='{$optionName}[]' 
                           value='{$catalogId}' 
                           id='{$optionName}_{$catalogId}' 
                           {$checked}
                    >
                    <label for='{$optionName}_{$catalogId}'>{$label}</label>
                </div>
HTML;
        }

        return $html;
    }


    /**
     * Возвращает все каталоги сайта
     *
     * @return array [
     *     'ID'               => ID инфоблока каталога
     *     'NAME'             => название каталога
     *     'IBLOCK_TYPE_ID'   => ID типа инфоблока каталога
     *     'IBLOCK_TYPE_NAME' => название типа инфоблока каталога
     * ]
     */
    public function getCatalogs(): array
    {
        try {
            $catalogIds = array_keys(CatalogHelper::getCatalogs());
            // убираем каталоги торговых предложений
            $catalogIds = array_filter($catalogIds, function($catalogId) {
                return !CatalogHelper::isSeparated($catalogId) || CatalogHelper::isProducts($catalogId);
            });

            // получаем название и тип ИБ для каталогов
            Loader::includeModule('iblock');
            return IblockTable::getList([
                'select' => [
                    'ID',
                    'NAME',
                    'IBLOCK_TYPE_ID',
                    'IBLOCK_TYPE_NAME' => 'TYPE.LANG_MESSAGE.NAME',
                ],
                'filter' => [
                    'TYPE.LANG_MESSAGE.LANGUAGE_ID' => LANG,
                    'ID'                            => $catalogIds
                ]
            ])->fetchAll();
        } catch (\Exception $e) {
            $this->addException($e);
            return [];
        }
    }



    /**
     * Возвращает вёрстку с селектом для типов цен
     *
     * @return string
     */
    public function getPriceTypesHtml(): string
    {
        $allTypes = $this->getPriceTypes();
        $selectedTypes = $this->getMinMaxPrices();

        $html = '<select multiple name="' . self::OPTION_MIN_MAX_PRICES . '[]">"';
        foreach ($allTypes as $id => $type) {
            $selected = in_array($id, $selectedTypes) ? 'selected' : '';
            $html .= "<option value='{$id}' {$selected}>{$type}</option>";
        }
        $html .= '</select>';

        return $html;
    }


    /**
     * Возвращает все типы цен
     *
     * @return array
     */
    public function getPriceTypes(): array
    {
        try {
            Loader::includeModule('catalog');
            $priceTypes = [];
            $priceTypesRes = GroupTable::getList([
                'select' => [
                    'NAME',
                    'ID',
                ],
            ]);
            while ($priceType = $priceTypesRes->fetch()) {
                $priceTypes[$priceType['ID']] = $priceType['NAME'];
            }

            return $priceTypes;
        } catch (LoaderException $e) {
            $this->addException($e);

            return [];
        } catch (SystemException $e) {
            $this->addException($e);

            return [];
        }
    }


    /**
     * Регистрирует служебные обработчики событий
     *
     * @param string $event
     */
    private function registerEvent(string $event)
    {
        $eventManager = EventManager::getInstance();
        switch ($event) {
            case 'price_change':
                if (CheckVersion(ModuleManager::getVersion('catalog'), '17.6.0')) {
                    $eventManager->registerEventHandler('catalog', \Bitrix\Catalog\Model\Price::class . '::' . DataManager::EVENT_ON_AFTER_ADD, self::getModuleId(), PriceEvents::class, 'onPriceAdd');
                    $eventManager->registerEventHandler('catalog', \Bitrix\Catalog\Model\Price::class . '::' . DataManager::EVENT_ON_AFTER_UPDATE, self::getModuleId(), PriceEvents::class, 'onPriceUpdate');
                } else {
                    $eventManager->registerEventHandler('catalog', 'OnPriceAdd', self::getModuleId(), PriceEvents::class, 'onPriceAddOld');
                    $eventManager->registerEventHandler('catalog', 'OnPriceUpdate', self::getModuleId(), PriceEvents::class, 'onPriceUpdateOld');
                }
                break;
        }
    }


    /**
     * Отменяет регистрирацию служебных обработчиков событий
     *
     * @param string $event
     */
    private function unregisterEvent(string $event)
    {
        $eventManager = EventManager::getInstance();
        switch ($event) {
            case 'price_change':
                if (CheckVersion(ModuleManager::getVersion('catalog'), '17.6.0')) {
                    $eventManager->unRegisterEventHandler('catalog', \Bitrix\Catalog\Model\Price::class . '::OnAfterAdd', self::getModuleId(), PriceEvents::class, 'onPriceAdd');
                    $eventManager->unRegisterEventHandler('catalog', \Bitrix\Catalog\Model\Price::class . '::OnAfterUpdate', self::getModuleId(), PriceEvents::class, 'onPriceUpdate');
                } else {
                    $eventManager->unRegisterEventHandler('catalog', 'OnPriceAdd', self::getModuleId(), PriceEvents::class, 'onPriceAddOld');
                    $eventManager->unRegisterEventHandler('catalog', 'OnPriceUpdate', self::getModuleId(), PriceEvents::class, 'onPriceUpdateOld');
                }
                break;
        }
    }


    public static function checkServerHash()
    {
        $options = new self();
        $lastHash = $options->getOption(self::OPTION_SERVER_HASH);
        $newHash = hash('sha256', serialize(Application::getConnection()->getConfiguration()));

        if ($lastHash !== $newHash) {
            $event = new Event(self::getModuleId(), self::EVENT_SERVER_CHANGED);
            $event->send();
            $options->saveOption(self::OPTION_SERVER_HASH, $newHash);
        }
    }
}
