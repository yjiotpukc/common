# Общая библиотека

Это модуль для Битрикса. В нём собран частый функционал наших сайтов.

- [Подсчёт минимальной и максимальной цен](/docs/recalc-prices.md)
- [Подсчёт продаж товаров](/docs/recalc-products-sales.md)
- [JavaScript-библиотеки](/docs/javascript.md)
- [Абстрактный класс для настроек модуля](/docs/abstract-options.md)
