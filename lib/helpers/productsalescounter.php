<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 10.09.18
 * Time: 11:09
 */

namespace Oneway\Common\Helpers;


use Bitrix\Main\Type\DateTime;
use Bitrix\Sale\Internals\BasketTable;
use Oneway\Common\Helpers\Catalog as CatalogHelper;
use Oneway\Common\Options;
use Oneway\Logger;


class ProductSalesCounter
{
    protected static $options;
    protected static $logger;

    protected $skusSold;
    protected $productsSold;
    /** @var array [productId => productIblockId] хранит принадлежность товара к инфоблоку */
    protected $productIblockIdMap = [];

    const PROPERTY_CODE = 'SALES_COUNT';


    public static function fullRecalc()
    {
        $counter = new static();
        $counter->resetAllProductsSales();
        $counter->recalcAllProductsSales();
        $counter->saveAllProductsSales();
    }


    public function saveAllProductsSales()
    {
        $options = self::getOptions();
        foreach ($options->getCatalogIds() as $catalogId) {
            $this->saveProductsSales($catalogId);
        }
    }


    public function recalcAllProductsSales()
    {
        $options = self::getOptions();
        foreach ($options->getCatalogIds() as $catalogId) {
            $this->recalcProductsSales($catalogId, 0, 0);
        }
    }


    public function resetAllProductsSales()
    {
        $options = self::getOptions();
        foreach ($options->getCatalogIds() as $catalogId) {
            $this->resetProductSales($catalogId, 0, 0);
        }
    }


    public function resetProductSales($catalogId, $limit, $step)
    {
        $propName = 'PROPERTY_' . static::PROPERTY_CODE;

        if (CatalogHelper::isSeparated($catalogId)) {
            $skuIblockId = CatalogHelper::getOfferIblockId($catalogId);
            $skusRes = \CIBlockElement::GetList(
                ['ID' => 'ASC'],
                [
                    'IBLOCK_ID'    => $skuIblockId,
                    "!{$propName}" => 0,
                ],
                false,
                [
                    'iNumPage'  => $step,
                    'nPageSize' => $limit,
                ],
                [
                    'ID',
                ]
            );

            while ($sku = $skusRes->Fetch()) {
                \CIBlockElement::SetPropertyValuesEx($sku['ID'], $catalogId, [
                    static::PROPERTY_CODE => 0,
                ]);
            }
        }

        $productsRes = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            [
                'IBLOCK_ID'    => $catalogId,
                "!{$propName}" => 0,
            ],
            false,
            [
                'iNumPage'  => $step,
                'nPageSize' => $limit,
            ],
            [
                'ID',
            ]
        );

        while ($product = $productsRes->Fetch()) {
            \CIBlockElement::SetPropertyValuesEx($product['ID'], $catalogId, [
                static::PROPERTY_CODE => 0,
            ]);
        }
    }


    public function recalcProductsSales($catalogId, $limit, $offset)
    {
        $filter = [];

        // добавляем фильтр по каталогам товаров и торговых предложений
        if (CatalogHelper::isSeparated($catalogId)) {
            if (CatalogHelper::isProducts($catalogId)) {
                $filter['IBLOCK_ID'] = [$catalogId, CatalogHelper::getOfferIblockId($catalogId)];
            } else {
                $filter['IBLOCK_ID'] = [$catalogId, CatalogHelper::getProductIblockId($catalogId)];
            }
        } else {
            $filter['IBLOCK_ID'] = $catalogId;
        }

        $options = self::getOptions();
        $days = $options->getOption(Options::OPTION_CALCULATE_PRODUCTS_SALES_PERIOD);
        if ((int) $days != 0) {
            $date = new DateTime();
            $date->setTime(0, 0, 0);
            $date->add("-{$days} days");

            $filter['>=DATE_INSERT'] = $date;
        }

        $basketsRes = BasketTable::getList([
            'filter' => $filter,
            'select' => [
                'PRODUCT_ID',
                'QUANTITY',
                // ИБ товара
                'IBLOCK_ID' => 'PRODUCT.IBLOCK.IBLOCK_ID'
            ],
            'order'  => ['ID' => 'ASC'],
            'limit'  => $limit,
            'offset' => $offset,
        ]);

        while ($basket = $basketsRes->fetch()) {
            if (isset($this->skusSold[$basket['PRODUCT_ID']])) {
                $this->skusSold[$basket['PRODUCT_ID']] += (int) $basket['QUANTITY'];
            } else {
                $this->skusSold[$basket['PRODUCT_ID']] = (int) $basket['QUANTITY'];
            }
            $this->productIblockIdMap[$basket['PRODUCT_ID']] = $basket['IBLOCK_ID'];
        }
    }


    public function recalcProductSales()
    {
        //todo
    }


    public function saveProductsSales($catalogId)
    {
        $hasSkus = CatalogHelper::isSeparated($catalogId);

        if (!$hasSkus) {
            $this->saveSkusSales($catalogId);
        } else {
            if (CatalogHelper::isProducts($catalogId)) {
                $productsIblockId = $catalogId;
                $skusIblockId = CatalogHelper::getOfferIblockId($catalogId);
            } else {
                $skusIblockId = $catalogId;
                $productsIblockId = CatalogHelper::getProductIblockId($catalogId);
            }

            $this->calculateParentProductsSales($skusIblockId);
            $this->saveSkusSales($skusIblockId);
            $this->saveParentProductsSales($productsIblockId);
        }
    }


    protected function saveSkusSales($iblockId)
    {
        $skus = array_filter($this->skusSold, function($sales, $skuId) use ($iblockId) {
            return $this->productIblockIdMap[$skuId] == $iblockId;
        }, ARRAY_FILTER_USE_BOTH);

        foreach ($skus as $skuId => $sales) {
            \CIBlockElement::SetPropertyValuesEx($skuId, $iblockId, [
                static::PROPERTY_CODE => $sales,
            ]);
        }
    }


    protected function saveParentProductsSales($iblockId)
    {
        $products = array_filter($this->productsSold, function($sales, $productId) use ($iblockId) {
            return $this->productIblockIdMap[$productId] == $iblockId;
        }, ARRAY_FILTER_USE_BOTH);

        foreach ($products as $productId => $sales) {
            \CIBlockElement::SetPropertyValuesEx($productId, $iblockId, [
                static::PROPERTY_CODE => $sales,
            ]);
        }
    }


    protected function calculateParentProductsSales($skusIblockId)
    {
        $productsRes = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            [
                'IBLOCK_ID' => $skusIblockId,
                '=ID'       => array_keys($this->skusSold),
            ],
            false,
            false,
            [
                'ID',
                'PROPERTY_CML2_LINK',
            ]
        );

        $productIblockId = CatalogHelper::getProductIblockId($skusIblockId);

        while ($product = $productsRes->Fetch()) {
            if (isset($this->productsSold[$product['PROPERTY_CML2_LINK_VALUE']])) {
                $this->productsSold[$product['PROPERTY_CML2_LINK_VALUE']] += $this->skusSold[$product['ID']];
            } else {
                $this->productsSold[$product['PROPERTY_CML2_LINK_VALUE']] = $this->skusSold[$product['ID']];
            }
            $this->productIblockIdMap[$product['PROPERTY_CML2_LINK_VALUE']] = $productIblockId;
        }
    }


    public function saveProductSales($productId, $catalogId, $sales)
    {
        $propName = 'PROPERTY_' . static::PROPERTY_CODE;
        \CIBlockElement::SetPropertyValuesEx($productId, $catalogId, [
            $propName => $sales,
        ]);
    }


    protected static function getLogger()
    {
        if (empty(self::$logger)) {
            self::$logger = new Logger('/exceptions/salesCounter.log');
        }

        return self::$logger;
    }


    protected static function getOptions()
    {
        if (empty(self::$options)) {
            self::$options = new Options();
        }

        return self::$options;
    }
}
