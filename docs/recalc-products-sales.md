# Подсчёт продаж товаров

Количество продаж товаров нужно для сортировки товаров по популярности.
Количество продаж хранится в свойстве SALES_COUNT.

## Как с этим работать?

В настройках модуля есть кнопка для пересчёта продаж.
Но перед этим нужно кое-что настроить.

Для начала выберем каталоги.
Выбирать нужно каталоги с товарами, а не торговыми предложениями.
Если есть только каталоги с товарами без торговых предложений, то работать будет так же хорошо.

Ставим галочку у "Рассчитывать количество продаж товара".
Заполняем по желанию "Период рассчёта". Если он не установлен, то продажи будут считаться за всё время.

Нажимаем кнопку "Пересчитать продажи" и радуемся, что можно программировать сайт дальше!
